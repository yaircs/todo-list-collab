import { ArgvParser } from "./modules/argv-parser.js";
import Controller from "./modules/controller.js";




const init = async () => {
    const commands = await ArgvParser.parse(process.argv);
    const controller = new Controller();
    await controller.start(commands);
};

init();